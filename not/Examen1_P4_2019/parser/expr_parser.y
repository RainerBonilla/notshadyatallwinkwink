%code requires{
    using YYSTYPE = double;
    #define YYSTYPE_IS_DECLARED 1
}

%{
    #include <unordered_map>
    #include "expr_lexer.h"
    #include <sstream>

    extern ExprLexer yylex;
    std::vector<double> map;

    #define YYERROR_VERBOSE 1

    void yyerror(const char* msg) {
        std::cerr << msg;
    }

    int getExprCount() {
        return map.size();
    }
    
    double getExprValue(int index) {
        return map[index];
    }
%}

%token TK_OPEN_PAR "("
%token NUMBER
%token OP_ADD "+"
%token OP_SUB "-"
%token TK_CLOSE_PAR ")"
%token OP_MUL "*"
%token OP_DIV "/"
%token SEMICOLON ";"
%token TK_EOF

%%

input: expr_list
;

expr_list: expr_list ";" expr
    | expr
    ;

expr: expr "+" term {$$ = $1 + $3;}
    | expr "-" term {$$ = $1 - $3;}
    | term          {$$ = $1;}
    ;

term: term "*" factor {$$ = $1 * $3;}
    | term "/" factor {$$ = $1 / $3;}
    | factor          {$$ = $1;}
    ;

factor: NUMBER {$$ = ;}
    | "(" expr ")" {$$ = $2;}
    ;
%%