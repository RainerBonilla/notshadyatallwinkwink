%option noyywrap
%option reentrant

%{

#include "expr_lexer.h"
int counter =0;
#define YY_EXTRA_TYPE std::istream*
#define YY_DECL Token ExprLexer::_getNextToken(yyscan_t yyscanner)
#define YY_INPUT(buf,result,max_size)\
    do{\
        std::istream* in = yyget_extra(yyscanner);\
        in->read(buf,max_size);\
        result = in->gcount();\
    }while(0)

%}

%x linecom
%x blockcom

%%
[ ] {}
[0-9]+|[0-9]+.[0-9]+ {text = yytext; return Token::Num;}
[_a-zA-Z]+[_a-zA-Z0-9]+ {text = yytext; return Token::Id;}
"+" {text = yytext; return Token::OpAdd;}
"-" {text = yytext; return Token::OpSub;}
"/" {text = yytext; return Token::OpDiv;}
"*" {text = yytext; return Token::OpMul;}
";" {text = yytext; return Token::Semicolon;}
"--" {BEGIN(linecom);}
"(*" {counter++; BEGIN(blockcom);}
"(" {text = yytext; return Token::OpenPar;}
")" {text = yytext; return Token::ClosePar;}
<INITIAL><<EOF>> {return Token::Eof;}
. { }

<linecom>\n {BEGIN(INITIAL);}
<linecom>. {}
<linecom><<EOF>> {return Token::Eof;}

<blockcom>"*)" {counter--; if(counter == 0) BEGIN(INITIAL);}
<blockcom>"(*" {counter++;};
<blockcom>. {}
<blockcom><<EOF>> {return Token::Eof;}

%%

ExprLexer::ExprLexer(std::istream &in):in(in) {
    yylex_init_extra(&in,&scanner);
}

ExprLexer::~ExprLexer() {
    yylex_destroy(scanner);
}
