#ifndef _EXPR_LEXER_H
#define _EXPR_LEXER_H

using yyscan_t = void*;

enum class Token{
    Decimal,
    Hex,
    Binary,
    Octal,
    Eof
};

class ExprLexer {
public:
    Token getNextToken() { return _getNextToken(scanner); }
    std::string getText() { return text; }
    int getLineNo();

private:
    /* Flex will generate this function */
    Token _getNextToken(yyscan_t yyscanner);

private:
    std::string text;
    yyscan_t scanner;
};
#endif
