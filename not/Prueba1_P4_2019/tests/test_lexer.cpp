#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include <iostream>
#include <sstream>
#include <string>
#include "doctest.h"
#include "expr_lexer.h"

std::string test = R"(0xdeadbeef (**** Comentario de Bloque
Line 2
Line 3
Line 4
************************)
12345678 # Line comment
0 0b1010 07654 a #Line comment
(* Block comment *))";

doctest::String toString(Token tk) {
    switch (tk) {
        case Token::Decimal: return "Decimal";
        case Token::Hex: return "Hex";
        case Token::Binary: return "Binary";
        case Token::Octal: return "Octal";
        case Token::Eof: return "Eof";
        default:
            return "Unknown";
    }
}

TEST_CASE("Lexer") {
    ExprLexer lexer(test);

    Token tk = lexer.getNextToken();
    std::string text = lexer.getText();
    
    CHECK(tk == Token::Hex);
    CHECK(text == "0xdeadbeef");
    
    tk = lexer.getNextToken();
    text = lexer.getText();
    CHECK(tk == Token::Decimal);
    CHECK(text == "12345678");
    
    tk = lexer.getNextToken();
    text = lexer.getText();
    CHECK(tk == Token::Decimal);
    CHECK(text == "0");
    
    tk = lexer.getNextToken();
    text = lexer.getText();
    CHECK(tk == Token::Binary);
    CHECK(text == "0b1010");
    
    tk = lexer.getNextToken();
    text = lexer.getText();
    CHECK(tk == Token::Octal);
    CHECK(text == "07654");
    
    try {
        tk = lexer.getNextToken();
        CHECK(false);
    } catch (std::string& s) {
        /* Nothing */
    }
    
    tk = lexer.getNextToken();
    CHECK(tk == Token::Eof);
}
