%option noyywrap
%option yylineno
%option reentrant

%{
#include <cstdio>
#include <iostream>
#include <sstream>
#include "expr_lexer.h"

#define YY_EXTRA_TYPE std::istream*
#define YY_DECL Token ExprLexer::_getNextToken(yyscan_t yyscanner)
#define YY_INPUT(buf, result, max_size)\
do{\
    std::istream* in = yyget_extra(yyscanner);\
    in->read(buf,max_size);\
    result = in->gcount();\
}while(0)

%}

%x blockChain

%%
[ ]
"0x"[0-9a-fA-F]+ {return Token::Hex;}
"0b"[0-1]+ {return Token::Binary;}
"0"[0-7]+ {return Token::Octal;}
[0-9]+ {return Token::Decimal;}
"#"[^\n]* { /**/ }
\n { }
<INITIAL><<EOF>> {return Token::Eof;}
"(*" { BEGIN(blockChain); }

<blockChain>"*)" { BEGIN(INITIAL); }
<blockChain>. { /**/ }
<blockChain>\n { /**/ }
<blockChain><<EOF>> { printf("ERROR: block comment not closed\n");  }
%%

ExprLexer::ExprLexer(std::ifstream &in) : in(in){
    yylex_init_extra(&in, &scanner); 
}

ExprLexer::~ExprLexer() {
    yylex_destroy(scanner);
}

int ExprLexer::getLineNo() {
    return yyget_lineno(scanner);
}