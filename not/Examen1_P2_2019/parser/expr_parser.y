%pure-parser

%code requires {
    using YYSTYPE = double;
    #define YYSTYPE_IS_DECLARED 1
}

%{
    #include "expr_parser.h"

    #define YYERROR_VERBOSE 1

    #define yylex(x) lexer.getNextToken(x)
    #define yyparse(x) ExprParser::parse()

    void ExprParser::yyerror(const char *msg) {
        cout << msg << endl;
    }
%}

%token NUMBER
%token SUB "-"
%token ADD "+"
%token MUL "*"
%token DIV "/"
%token OPENPAR "("
%token CLOSEPAR ")"

%%

expr: expr "+" term {$$ = $1 + $3;}
    | expr "-" term {$$ = $1 - $3;}
    | term {$$ = $1;}
    ;

term: term "*" factor {$$ = $1 * $3;}
    | term "/" factor {$$ = $1 / $3;}
    | factor {$$ = $1;}
    ;

factor: NUMBER {double valor; lexer.getNextToken(&valor); $$ = valor;}
    ;