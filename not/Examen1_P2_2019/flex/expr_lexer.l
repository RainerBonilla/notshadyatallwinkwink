%option noyywrap
%option reentrant

%{
    #include "expr_lexer.h"

    #define YY_EXTRA_TYPE std::istream*
    #define YY_DECL Token ExprLexer::_getNextToken(yyscan_t yyscanner)
    #define YY_INPUT(buf, result, max_size)\
        do {\
            istream *in = yyget_extra(yyscanner);\
            in->read(buf, max_size); \
            result = in->gcount();\
        } while(0)

%}

%x BCOMMENT

DIGIT [0-9]
LETTER [a-zA-Z]

%%

{DIGIT}+(.{DIGIT}+)? text = yytext; return Token::Num;
({LETTER}|_)({DIGIT}|{LETTER}|_)+ text = yytext; return Token::Id;
"(" text = yytext; return Token::OpenPar;
")" text = yytext; return Token::ClosePar;
"+" text = yytext; return Token::OpAdd;
"-" text = yytext; return Token::OpSub;
; text = yytext; return Token::Semicolon;
--[^\n]* BEGIN(INITIAL);
[ \n] /* */
"(*" BEGIN(BCOMMENT);
<INITIAL><<EOF>> return Token::Eof;

<BCOMMENT>. /* */
<BCOMMENT>\n /* */
<BCOMMENT>"*)" BEGIN(INITIAL);
<BCOMMENT><<EOF>> return Token::Eof;

%%

ExprLexer::ExprLexer(istream& in) : in(in){
    yylex_init_extra(&in, &scanner);
}

ExprLexer::~ExprLexer() {

}