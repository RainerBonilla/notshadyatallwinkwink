#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include <fstream>
#include "doctest.h"
#include "expr_ast.h"

#define ADD(x, y) (new AddExpr((x), (y)))
#define SUB(x, y) (new SubExpr((x), (y)))
#define MUL(x, y) (new MulExpr((x), (y)))
#define DIV(x, y) (new DivExpr((x), (y)))
#define MOD(x, y) (new ModExpr((x), (y)))
#define NUM(n) (new NumExpr((n)))
#define ID(x) (new IdentExpr(#x))

std::string runCmd(const std::string& cmd) {
    FILE *stream = popen( cmd.c_str(), "r" );
    REQUIRE (stream != nullptr);

    std::ostringstream ssdata;
    char buffer[256] = {0};

    while(fgets(buffer, sizeof(buffer)-1, stream) != nullptr) {
        ssdata << buffer;
    }
    return ssdata.str();
}

void testCodeGen(const char *filename, ASTNode *n, const std::string& extraCode, int val, int expOffset) {
    resetCodegen();
    n->genCode();

    std::string code = n->code;
    REQUIRE( getOffset() <= expOffset );

    std::ofstream out(filename, std::ios::out|std::ios::trunc);

    REQUIRE (out.is_open());
    out << code << "\n" << extraCode;
    out.close();

    std::string cmd = std::string("nasm -felf32 -otest.o ") + filename + " 2>&1";
    std::string nasmOutput = runCmd(cmd.c_str());
    if (!nasmOutput.empty()) {
        std::cout << "\x1b[0;31mNASM Output:\x1b[0m" <<nasmOutput;
    }
    REQUIRE( nasmOutput.empty() );

    std::string gccOutput = runCmd("gcc -m32 -otest test.o 2>&1");
    if (!gccOutput.empty()) {
        std::cout << "\x1b[0;35mGCC Output:\x1b[0m" << gccOutput;
    }
    int gccError = gccOutput.find("error:");
    REQUIRE(gccError == static_cast<int>(std::string::npos));

    std::string output = runCmd("./test");
    REQUIRE(!output.empty());
    int result = std::stol(output);

    CHECK(result == val);
}

TEST_CASE("Add expression") {
    ASTNode *e = ADD(ADD(ID(x), ID(y)), NUM(300));
    
    testCodeGen("add.asm", new FuncNode(e), "section .data\nx dd 100\ny dd 200\n", 600, 12);
}

TEST_CASE("Sub expression") {
    ASTNode *e = SUB(SUB(ID(x), ID(y)), NUM(100));
    
    testCodeGen("sub.asm", new FuncNode(e), "section .data\nx dd 100\ny dd 200\n", -200, 12);
}

TEST_CASE("Mult expression") {
    ASTNode *e = MUL(MUL(ID(x), NUM(10)), ID(y));
    
    testCodeGen("mul.asm", new FuncNode(e), "section .data\nx dd 33\ny dd 5\n", 1650, 12);
}

TEST_CASE("Div expression") {
    ASTNode *e = DIV(DIV(ID(x), NUM(10)), ID(y));
    
    testCodeGen("div.asm", new FuncNode(e), "section .data\nx dd 330\ny dd 5\n", 6, 12);
}

TEST_CASE("Mod expression") {
    ASTNode *e = MOD(ID(x), NUM(5));
    
    testCodeGen("div.asm", new FuncNode(e), "section .data\nx dd 33\ny dd 5\n", 3, 8);
}

TEST_CASE("Combined expression (Add/Sub)") {
    ASTNode *e = MUL(ADD(NUM(10), ID(y)), SUB(ID(x), NUM(580)));
    
    testCodeGen("add_sub.asm", new FuncNode(e), "section .data\nx dd 33\ny dd 5\n", -8205, 20);
}

TEST_CASE("Combined expression (Div/Mod)") {
    ASTNode *e = MOD(DIV(ADD(ID(x), NUM(10)), ID(y)), NUM(13));
    
    testCodeGen("div_mod.asm", new FuncNode(e), "section .data\nx dd 3300\ny dd 5\n", 12, 20);
}
